Der Lostopf ist eine Webseite, die bei der Erstellung einer Redeliste für eine Veranstaltung unterstützt. Programmiert von Martin Tille.

Die Seite benötigt den Grünen SAML-Login, um Präsidium und Teilnehmende (Delegierte) der Veranstaltung zu unterscheiden. 

Beim Lostopf wird zwischen Redeplätzen für Frauen und offenen Redeplätzen unterschieden, um eine Redeliste nach Geschlecht alternierend zu erzeugen.

Die Auswahl aus dem Lostopg geschieht über einen Zufallsgenerator, der einen Eintrag aus dem Lostopf auswirft. 