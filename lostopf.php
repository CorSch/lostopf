<?php
//## TODO 3. Präsidiumsmitglied?

// Festlegung wer bei uns über SAML angemeldet ist und im Präsidium sitzt - uidNumber 
// 123456=Michael, 654321=Silke, 34567=Gesine, Zahlen sind erfunden
$praesidium = array(123456,654321,34567);

// Hier kommen alle Delegierte, Ersatzdelegierte und angemeldete Teilnehmer rein
$abstimmende = array();

// Initialisiere die Anzeige der HTML Seite
$template = 0;
$praetemplate = 0;
$templatenein = 0;

// Zugriff auf Datenbank initialisieren
$dbhandle = new PDO('sqlite:db.sqlite');
$dbhandle->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
$dbhandle->exec("CREATE TABLE IF NOT EXISTS lostopf (id INTEGER PRIMARY KEY AUTOINCREMENT, uidnumber INT, vorname TEXT, nachname TEXT, gliederung TEXT, lostopf SMALLINT)");
$dbhandle->exec("CREATE TABLE IF NOT EXISTS top (id INTEGER PRIMARY KEY AUTOINCREMENT, top TEXT)");
$dbhandle->exec("CREATE TABLE IF NOT EXISTS gezogene (id INTEGER PRIMARY KEY AUTOINCREMENT, top TEXT, vorname TEXT, nachname TEXT, gliederung TEXT, lostopf SMALLINT)");

// Zugriff auf SAML initialisieren
require_once('/var/simplesaml/lib/_autoload.php');
$as = new SimpleSAML_Auth_Simple('default-sp');

// Umleitung zu HTTPS falls noch HTTP
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://service.gruene.de' . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
}

// Wenn bereits per SAML angemeldet, sonst anmelden (heisst es gibt keine "Vorschalt-Loginseite")
if ( $as->isAuthenticated() ) {
  // Übergebene Attribute auslesen
  $samlattributes = $as->getAttributes();
  if (isset($samlattributes)) {
    // Zuweisen der SAML Infromationen
    $uidnumber = $samlattributes['uidNumber'][0];
    $vorname = $samlattributes['urn:oid:2.5.4.42'][0];
    $nachname = $samlattributes['urn:oid:2.5.4.4'][0];
    $gliederung = $samlattributes['membershipOrganizationMainName'][0];
    // Sollte es ein Präsidiumsmitglied sein
    if ( in_array($uidnumber,$praesidium) ) {
      // "Aktiviere" Präsidumsansicht
      $praetemplate = 1;
      // Sollte auf ein Lostopf gedrückt worden sein, Datenbank je nach Typ abfragen.
      if ( isset($_POST['ziehungfrauentopf']) || isset($_POST['ziehungfrauentopf_x']) ) {
        $dataarray = $dbhandle->query("SELECT * FROM lostopf WHERE lostopf = 1");
        $gezogenertyp = "Frauentopf";
      }
      if ( isset($_POST['ziehungoffenertopf']) || isset($_POST['ziehungoffenertopf_x']) ) {
        $dataarray = $dbhandle->query("SELECT * FROM lostopf WHERE lostopf = 2");
        $gezogenertyp = "Offenen Topf";
      }
      if ( isset($_POST['ziehungfrauentopf']) || isset($_POST['ziehungfrauentopf_x']) || isset($_POST['ziehungoffenertopf']) || isset($_POST['ziehungoffenertopf_x']) ) {
        // So Ergbnisse holen
        $daten = $dataarray->fetchAll();
        // Wenn noch was im Lostopf drin, dann
        if (!empty($daten)) {
          // Einen rausholen (besser wäre die Funktion random_int, aber ich hab hier PHP < 7.0)
          $gezogen = rand(0,count($daten)-1);
          // Daten zwischenspeichern.
          $gezogenvorname = $daten[$gezogen]['vorname'];
          $gezogennachname = $daten[$gezogen]['nachname'];
          $gezogengliederung = $daten[$gezogen]['gliederung'];
          $gezogenlostopf = $daten[$gezogen]['lostopf'];
          // Anzeige des Ergebnisses aktivieren
          $praetemplate = 2;
          // Eintrag aus dem "Lostopf" löschen
          $dbhandle->query("DELETE FROM lostopf WHERE id = ".$daten[$gezogen]['id']);

          // Datenbank nach aktuellem TOP abfragen
          $dataarray = $dbhandle->query("SELECT * FROM top ORDER BY id DESC;");
          $daten = $dataarray->fetchAll();
          // Wenn einer gesetzt (sollte eigentlich nicht möglich sein jemand zu ziehen ohne das ein TOP definiert ist)
          if (!empty($daten)) {
            $aktuellertop = $daten[0]['top'];
            // Schreibe Gezogene samt TOP zusätzlich in extra Tabelle
            $sql = "INSERT INTO gezogene (top, vorname, nachname, gliederung, lostopf) VALUES (:top, :vorname, :nachname, :gliederung, :lostopf)";
            $sth = $dbhandle->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute(array(':top' => $aktuellertop, ':vorname' => $gezogenvorname, ':nachname' => $gezogennachname, ':gliederung' => $gezogengliederung, ':lostopf' => $gezogenlostopf ));
          }
        }
      }
      // Neuen TOP in DB Eintragen.
      // ## Reload Sperre
      if (isset($_POST['neuertop'])) {
        $sql = "INSERT INTO top (top) VALUES (:top)";
        $sth = $dbhandle->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array(':top' => $_POST['top'] ));
        // Alles aus Lostopf entfernen wenn neuer TOP aufgerufen wird.
        $dbhandle->query("DELETE FROM lostopf WHERE id >= 0");
      }
    }
    if ( !in_array($uidnumber,$abstimmende) ) {
      $templatenein = 1;
    }
  }
} else {
  $as->requireAuth();
}

// Wenn sich jemand in einen der Töpfe eingeworfen hat, Eintrag in die Datenbank. Losopf 1 = Frauentopf, Lostopf 2 = Offener Topf
if (isset($_POST['frauentopf']) || isset($_POST['frauentopf_x']) ) {
  $sql = "INSERT INTO lostopf (uidnumber, vorname, nachname, gliederung, lostopf) VALUES (:uidnumber, :vorname,:nachname,:gliederung,1)";
}
if (isset($_POST['offenertopf']) || isset($_POST['offenertopf_x']) ) {
  $sql = "INSERT INTO lostopf (uidnumber, vorname, nachname, gliederung, lostopf) VALUES (:uidnumber, :vorname,:nachname,:gliederung,2)";
}

// Sql vorbereitet, jetzt ausführen wenn nicht schon eingeworfen.
if ( isset($_POST['frauentopf']) || isset($_POST['frauentopf_x']) || isset($_POST['offenertopf']) || isset($_POST['offenertopf_x']) ) {
  $dataarray = $dbhandle->query("SELECT uidnumber FROM lostopf WHERE uidnumber = ".$uidnumber);
  $daten = $dataarray->fetchAll();
  if (empty($daten)) { 
    $sth = $dbhandle->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $sth->execute(array(':uidnumber' => $uidnumber, ':vorname' => $_POST['vorname'], ':nachname' => $_POST['nachname'], ':gliederung' => $_POST['gliederung'] ));
  }
}

// Anzahl an Einwürfen abfragen
$dataarray = $dbhandle->query("SELECT COUNT(lostopf) FROM lostopf WHERE lostopf = 1");
$daten = $dataarray->fetchAll();
$anzimfrauen = ($daten[0]['COUNT(lostopf)']);
$dataarray = $dbhandle->query("SELECT COUNT(lostopf) FROM lostopf WHERE lostopf = 2");
$daten = $dataarray->fetchAll();
$anzimoffenen = ($daten[0]['COUNT(lostopf)']);

// Prüfe ob sich ein Mitglied schon eingeworfen hat, falls ja Seite mit "einwerfen" nicht mehr zeigen.
$dataarray = $dbhandle->query("SELECT uidnumber FROM lostopf WHERE uidnumber = ".$uidnumber);
$daten = $dataarray->fetchAll();
if (empty($daten)) { $template = 1; } else { $template = 2; }

// Datenbank nach TOP abfragen
$dataarray = $dbhandle->query("SELECT * FROM top ORDER BY id DESC;");
$daten = $dataarray->fetchAll();
if (empty($daten)) { 
  // zeige "es gibt keinen TO"
  $template = 3;
  // Falls Präsidium Zeige TOP Einwurf
  if ($praetemplate > 0) { $praetemplate = 3; };
  
} else {
  $aktuellertop = $daten[0]['top'];
}

//Zugriff auf Datenbank schließen
$dbhandle = null;

// Wenn nicht berechtigt fürs Einwerfen, dann
if ($templatenein == 1) {
  $template = 0;  
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Anmeldewebseite für den Länderrat">
    <meta name="author" content="Martin Tille">
    <title>Lostopf 1. Länderrat 2020 von Bündnis 90/Die Grünen</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" >

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="icon" href="favicon.ico">
    <meta name="theme-color" content="#563d7c">

    <link rel="stylesheet" href="cover.css">

    <style type="text/css">
      .hovernumber { 
        position: absolute;
        margin-top: 2vw;
        margin-left: 10vw;
        font-size: 16px;
        color: #fff;
        text-align: center;
        text-transform: uppercase;
        background-color: #e6007e;
        border-radius: 15px;
        padding-left: 5px;
        padding-right: 5px;
        padding-bottom: 5px;
        width: 30px;
        height: 30px;
        padding-top: 3px;
        border-radius: 100%;
      }
    </style>
  </head>
  
  <body>
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <img src="GRUENE_Parteitagslogo_RGB.svg">
<?php if ( $praetemplate > 0 ) { ?>
        <p class="lead" style="border: 10px solid #A00; padding-left: 8px;">Mit Präsidiumsansicht!</p>
<?php } ?>
      </header>
  
      <main role="main" class="inner cover">
      <h2>Herzlich willkommen <br/>zum digitalen Lostopf für den 1. Länderrat 2020.</h2>
<?php if ( $templatenein == 1) { ?>
        <div class="row">
          <div class="col"><p class="lead">Du hast nicht für den Länderrat angemeldet und kannst daher leider nicht für einen Redebeitrag einwerfen. Sollte es sich dabei um ein Fehler handeln wende Dich bitte an gremien@gruene.de.</p></div>          
        </div>
<?php } ?>      
<?php if ( $template == 1 ) { ?>
      <form enctype="multipart/form-data" action="index.php" method="post">
        <div class="row">
          <div class="col"><p style="font-size: 18px;">Wenn Du Mitglied bist kannst Du Dich hier für einen Redebeitrag zur Debatte: "<?php echo($aktuellertop); ?>" einwerfen. Trag dazu Deinen Vor- und Nachnamen sowie Deinen Kreisverband ein und klicke auf den für Dich zutreffenden Lostopf.</p></div>          
        </div>
        <div class="row row-cols-2">
          <div class="col"><input type="text" class="form-control" placeholder="Vorname" name="vorname" value="<?php if (isset($vorname)) { echo $vorname;} ?>"></div>
          <div class="col"><input type="text" class="form-control" placeholder="Nachname" name="nachname" value="<?php if (isset($vorname)) { echo $nachname;} ?>"></div>
        </div>
        <div class="row">
          <div class="col"><input type="text" class="form-control" placeholder="Kreis/Ortsverband" name="gliederung" value="<?php if (isset($gliederung)) { echo $gliederung;} ?>"></div>
        </div>
        <div class="row row-cols-2">
          <div class="col">
<!--            <input class="btn btn-warning btn-block" type=submit name="frauentopf" value="Frauenlostopf"> -->
            <span class="hovernumber"><?php echo($anzimfrauen); ?></span>
            <input type=image src="DigitalerParteitag_Button_Lostopf_Frauen.svg" name="frauentopf" alt="Für den Frauenlostopf einwerfen" onMouseOver="this.src='DigitalerParteitag_Button_Lostopf_Frauen_hover.svg'" onMouseOut="this.src='DigitalerParteitag_Button_Lostopf_Frauen.svg'">
          </div>
          <div class="col">
<!--            <input class="btn btn-warning btn-block" type=submit name="offenertopf" value="Offener Lostopf"> -->
            <span class="hovernumber"><?php echo($anzimoffenen); ?></span>
            <input type=image src="DigitalerParteitag_Button_Lostopf_Offen.svg" name="offenertopf" alt="Für den Offenen Lostopf einwerfen" onMouseOver="this.src='DigitalerParteitag_Button_Lostopf_Offen_hover.svg'" onMouseOut="this.src='DigitalerParteitag_Button_Lostopf_Offen.svg'">
          </div>
        </div>
        <div class="row">
          <div class="col"><p>Aktuell gibt es <?php echo($anzimfrauen); ?> Meldungen im Frauenlostopf und <?php echo($anzimoffenen); ?> im Offenen Lostopf.</p><a href="https://service.gruene.de/lostopf/gezogene.php" target="_blank" class="btn btn-lg btn-success">Hier gibts die Liste aller gezogenen Einträge.</a></div>
        </div>
       </form>
<?php } ?>
<?php if ( $template == 2) { ?>
        <div class="row">
          <div class="col"><p class="lead">Du hast Dich bereits zur Debatte: "<?php echo($aktuellertop); ?>" eingeworfen.</p></div>          
        </div>
        <div class="row">
          <div class="col"><p>Aktuell gibt es <?php echo($anzimfrauen); ?> Meldungen im Frauenlostopf und <?php echo($anzimoffenen); ?> im Offenen Lostopf.</p><a href="https://service.gruene.de/lostopf/gezogene.php" target="_blank" class="btn btn-lg btn-success">Hier gibts die Liste aller gezogenen Einträge.</a></div>
        </div>
<?php } ?>
<?php if ( $template == 3) { ?>
        <div class="row">
          <div class="col"><p class="lead">Es ist noch kein Tagesordnungspunkt hinterlegt für den Du Dich einwerfen kannst.</p></div>          
        </div>
<?php } ?>
<?php if ( $praetemplate > 0 ) { // Beginn aller Präsidiumsansichten ?>
      <form enctype="multipart/form-data" action="index.php" method="post"><div>
<?php if ($praetemplate == 2) { ?>
        <div class="row" style="border: 10px solid #A00; padding-left: 8px;">
          <div class="col"><p class="lead">Es wurde aus dem <?php echo($gezogenertyp." gezogen: ".$gezogenvorname." ".$gezogennachname." aus der Gliederung: ".$gezogengliederung); ?>  .</p></div>
        </div>
<?php } ?>
<?php if ( ($praetemplate == 1) || ($praetemplate == 2) ) { ?>
        <div style="border: 10px solid #A00; padding-left: 8px; margin-bottom: 8px;">
          <div class="row">
            <div class="col"><p>Durch klicken auf den entsprechenden Lostopf wird ein Eintrag gezogen.</p></div>
          </div>
          <div class="row row-cols-2">
            <div class="col">
              <input class="btn btn-warning" type=submit name="ziehungfrauentopf" value="Ziehe aus dem Frauenlostopf">
            </div>
            <div class="col">
              <input class="btn btn-warning" type=submit name="ziehungoffenertopf" value="Ziehe aus dem Offenen Lostopf">
            </div>
          </div>
        </div>
        <div style="border: 10px solid #A00; padding-left: 8px; margin-bottom: 8px;">
          <div class="row">
            <div class="col"><p class="lead">Hier kann der nächste Debatttenpunkt festgelegt werden.</p></div>
          </div>
          <div class="row">
            <div class="col"><input type="text" class="form-control" placeholder="Bitte hier den nächsten Debattenpunkt eintragen." name="top"></div>          
          </div>
          <div class="row">
            <div class="col"><input class="btn btn-warning" type=submit name="neuertop" value="Neuen Debattenpunkt festlegen, dabei werden alle Lostöpfe geleert."></div>
          </div>        
        </div>
<?php } ?>
<?php if ($praetemplate == 3) { ?>
        <div class="row">
          <div class="col"><p class="lead">Liebes Präsidium, bitte lege einen Debattenpunkt fest.</p></div>
        </div>
        <div class="row">
          <div class="col"><input type="text" class="form-control" placeholder="Bitte hier einen Debattenpunkt eintragen." name="top"></div>          
        </div>
        <div class="row">
          <div class="col"><input class="btn btn-warning" type=submit name="neuertop" value="Ersten TOP festlegen"></div>
        </div>
<?php } ?>
      </div></form>
<?php } // Ende Präsidiumsansicht ?>
      </main>
      <footer class="mastfoot mt-auto">
      </footer>      
    </div>
  </body>
</html>